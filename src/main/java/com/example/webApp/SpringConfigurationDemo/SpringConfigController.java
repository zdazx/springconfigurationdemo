package com.example.webApp.SpringConfigurationDemo;

import com.example.webApp.SpringConfigurationDemo.domain.User;
import com.example.webApp.SpringConfigurationDemo.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class SpringConfigController {

    @Autowired
    UserRepository repo;

    @Value("${twuc.webapp.greet}")
    private String greet;

    @GetMapping("/api/hello")
    public String sayHelloWorld(){
        return greet;
    }

    @PostMapping("/api/user")
    public ResponseEntity createUser(@RequestBody User user){
        User savedUser = repo.save(user);
        return ResponseEntity.status(201)
                .header("Location", "http://localhost/api/user/"+savedUser.getId())
                .build();
    }

    @GetMapping("/api/user/{id}")
    public ResponseEntity getUser(@Valid @PathVariable Long id){
        User savedUser = repo.findById(id).orElseThrow(RuntimeException::new);
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(savedUser);
    }
}
