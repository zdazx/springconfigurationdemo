package com.example.webApp.SpringConfigurationDemo;

import com.example.webApp.SpringConfigurationDemo.domain.User;
import com.example.webApp.SpringConfigurationDemo.domain.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class SpringConfigControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    UserRepository repo;

    @Test
    public void should_test_configuration_can_be_used_in_two_environments() throws Exception {
        mockMvc.perform(get("/api/hello"))
                .andExpect(content().string("HelloWorldTest"));
    }

    @Test
    void should_test_create_user() throws Exception {
        mockMvc.perform(createUser(new User(1L, "ddd")))
                .andExpect(status().is(201))
                .andExpect(header().string("Location", "http://localhost/api/user/1"));
    }

    @Test
    void should_get_user() throws Exception {
        mockMvc.perform(createUser(new User(1L, "ddd")))
        .andExpect(status().is(201));

        User user = repo.findById(1l).orElseThrow(RuntimeException::new);
        assertEquals(Long.valueOf(1L), user.getId());
        assertEquals("ddd", user.getName());
    }

    private MockHttpServletRequestBuilder createUser(User user) throws JsonProcessingException {
        return post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper()
                        .writeValueAsString(user));
    }
}
